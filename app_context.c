#include "app_context.h"

void initialize_app_context(app_context *app_ctx, SSL_CTX *ssl_ctx, struct event_base *evbase) {
	memset(app_ctx, 0, sizeof(app_context));
	app_ctx->ssl_ctx = ssl_ctx;
	app_ctx->evbase = evbase;
}
