#ifndef __APP_CONTEXT_H__
#define __APP_CONTEXT_H__

#include <openssl/ssl.h>

typedef struct app_context {
  SSL_CTX *ssl_ctx;
  struct event_base *evbase;
} app_context;

void initialize_app_context(app_context *app_ctx, SSL_CTX *ssl_ctx, struct event_base *evbase);

#endif
