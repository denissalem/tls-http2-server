static void cb_read(struct bufferevent *bev, void *ptr);
static void cb_write(struct bufferevent *bev, void *ptr);
static void cb_event(struct bufferevent *bev, short events, void *ptr);
