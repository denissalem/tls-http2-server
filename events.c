#include <err.h>

#include "http2.h" 
#include "events.h"
#include "events-static.h"

/* callback for bufferevent after client connection header was checked. */
static void cb_read(struct bufferevent *bev, void *ptr) {
  	(void) bev;
	http2_session_data *session_data = (http2_session_data *) ptr;

	if (session_recv(session_data) != 0) {
		delete_http2_session_data(session_data);
		return;
	}
}

/* writecb for bufferevent. To greaceful shutdown after sending or
   receiving GOAWAY, we check the some conditions on the nghttp2
   library and output buffer of bufferevent. If it indicates we have
   no business to this session, tear down the connection. If the
   connection is not going to shutdown, we call session_send() to
   process pending data in the output buffer. This is necessary
   because we have a threshold on the buffer size to avoid too much
   buffering. See send_callback(). */
static void cb_write(struct bufferevent *bev, void *ptr) {
	http2_session_data *session_data = (http2_session_data *)ptr;
	if (evbuffer_get_length(bufferevent_get_output(bev)) > 0) {
		return;
	}
	if (nghttp2_session_want_read(session_data->session) == 0 && nghttp2_session_want_write(session_data->session) == 0) {
		delete_http2_session_data(session_data);
    		return;
	}
	if (session_send(session_data) != 0) {
		delete_http2_session_data(session_data);
		return;
	}
}

/* eventcb for bufferevent */
static void cb_event(struct bufferevent *bev, short events, void *ptr) {
    	(void)bev;
	http2_session_data *session_data = (http2_session_data *)ptr;
	if (events & BEV_EVENT_CONNECTED) {
		const unsigned char *alpn = NULL;
		unsigned int alpnlen = 0;
		SSL *ssl;
		fprintf(stderr, "%s connected\n", session_data->client_addr);
		ssl = bufferevent_openssl_get_ssl(session_data->bev);
		SSL_get0_next_proto_negotiated(ssl, &alpn, &alpnlen);
		if (alpn == NULL) {
			SSL_get0_alpn_selected(ssl, &alpn, &alpnlen);
		}

		if (alpn == NULL || alpnlen != 2 || memcmp("h2", alpn, 2) != 0) {
			fprintf(stderr, "%s h2 is not negotiated\n", session_data->client_addr);
			delete_http2_session_data(session_data);
			return;
		}

		initialize_nghttp2_session(session_data);

		if (send_server_connection_header(session_data) != 0 ||
			session_send(session_data) != 0) {
	  		delete_http2_session_data(session_data);
			return;
		}
		return;
	}

  	if (events & BEV_EVENT_EOF) {
		fprintf(stderr, "%s EOF\n", session_data->client_addr);
	} else if (events & BEV_EVENT_ERROR) {
		fprintf(stderr, "%s network error\n", session_data->client_addr);
	} else if (events & BEV_EVENT_TIMEOUT) {
		fprintf(stderr, "%s timeout\n", session_data->client_addr);
	}
	delete_http2_session_data(session_data);
}

/* callback for evconnlistener */
void cb_accept(struct evconnlistener *listener, int fd, struct sockaddr *addr, int addrlen, void *arg) {
	(void) listener;
	app_context *app_ctx = (app_context *)arg;
	http2_session_data *session_data;
	session_data = create_http2_session_data(app_ctx, fd, addr, addrlen);
	bufferevent_setcb(session_data->bev, cb_read, cb_write, cb_event, session_data);
}

void server_listen(struct event_base *evbase, const char *service, app_context *app_ctx) {
	struct addrinfo hints;
	struct addrinfo *res, *rp;

	memset(&hints, 0, sizeof(hints));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	#ifdef AI_ADDRCONFIG
  	hints.ai_flags |= AI_ADDRCONFIG;
	#endif /* AI_ADDRCONFIG */

	if (getaddrinfo(NULL, service, &hints, &res) != 0) {
		errx(1, "Could not resolve server address");
	}
	for (rp = res; rp; rp = rp->ai_next) {
		struct evconnlistener *listener;
		listener = evconnlistener_new_bind(evbase, cb_accept, app_ctx, LEV_OPT_CLOSE_ON_FREE | LEV_OPT_REUSEABLE,16, rp->ai_addr, (int)rp->ai_addrlen);
		if (listener) {
			freeaddrinfo(res);
			return;
		}
	}
	errx(1, "Could not start listener");
}

