#include <event.h>
#include <event2/bufferevent_ssl.h>
#include <event2/listener.h>

#include "app_context.h"

void cb_accept(struct evconnlistener *listener, int fd, struct sockaddr *addr, int addrlen, void *arg);
void server_listen(struct event_base *evbase, const char *service, app_context *app_ctx);
