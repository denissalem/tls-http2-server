#include "http2.h" 

#define OUTPUT_WOULDBLOCK_THRESHOLD (1 << 16)

#define MAKE_NV(NAME, VALUE)                                                   \
  {                                                                            \
    (uint8_t *)NAME, (uint8_t *)VALUE, sizeof(NAME) - 1, sizeof(VALUE) - 1,    \
        NGHTTP2_NV_FLAG_NONE                                                   \
  }

#define ARRLEN(x) (sizeof(x) / sizeof(x[0]))


static const char ERROR_HTML[] = "<html><head><title>404</title></head>"
                                 "<body><h1>404 Not Found biatch!</h1></body></html>";

static void delete_http2_stream_data(http2_stream_data *stream_data);
static ssize_t cb_send(nghttp2_session *session, const uint8_t *data, size_t length, int flags, void *user_data);
static int cb_on_frame_recv(nghttp2_session *session, const nghttp2_frame *frame, void *user_data);
static int on_request_recv(nghttp2_session *session, http2_session_data *session_data, http2_stream_data *stream_data);
static int error_reply(nghttp2_session *session, http2_stream_data *stream_data);
static int send_response(nghttp2_session *session, int32_t stream_id, nghttp2_nv *nva, size_t nvlen, int fd);
static ssize_t cb_file_read(nghttp2_session *session, int32_t stream_id, uint8_t *buf, size_t length, uint32_t *data_flags, nghttp2_data_source *source, void *user_data);
static int check_path(const char *path);
static int ends_with(const char *s, const char *sub);
static int cb_on_stream_close(nghttp2_session *session, int32_t stream_id, uint32_t error_code, void *user_data);
static void remove_stream(http2_session_data *session_data, http2_stream_data *stream_data);
static int cb_on_header(nghttp2_session *session, const nghttp2_frame *frame, const uint8_t *name, size_t namelen, const uint8_t *value, size_t valuelen, uint8_t flags, void *user_data);
static char *percent_decode(const uint8_t *value, size_t valuelen);
static uint8_t hex_to_uint(uint8_t c);
static int cb_on_begin_headers(nghttp2_session *session, const nghttp2_frame *frame, void *user_data);
static http2_stream_data * create_http2_stream_data(http2_session_data *session_data, int32_t stream_id);
static void add_stream(http2_session_data *session_data, http2_stream_data *stream_data);
