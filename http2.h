#ifndef __HTTP2_H__
#define __HTTP2_H__

#include <nghttp2/nghttp2.h>
#include <sys/socket.h>

#include "app_context.h"

#define OUTPUT_WOULDBLOCK_THRESHOLD (1 << 16)

#define MAKE_NV(NAME, VALUE)                                                   \
  {                                                                            \
    (uint8_t *)NAME, (uint8_t *)VALUE, sizeof(NAME) - 1, sizeof(VALUE) - 1,    \
        NGHTTP2_NV_FLAG_NONE                                                   \
  }

#define ARRLEN(x) (sizeof(x) / sizeof(x[0]))

typedef struct http2_stream_data {
	struct http2_stream_data *prev, *next;
	char *request_path;
	int32_t stream_id;
	int fd;
} http2_stream_data;

typedef struct http2_session_data {
	struct http2_stream_data root;
	struct bufferevent *bev;
	app_context *app_ctx;
	nghttp2_session *session;
	char *client_addr;
} http2_session_data;


http2_session_data *create_http2_session_data(app_context *app_ctx, int fd, struct sockaddr *addr, int addrlen);
int session_recv(http2_session_data *session_data);
void delete_http2_session_data(http2_session_data *session_data);
void initialize_nghttp2_session(http2_session_data *session_data);
int session_send(http2_session_data *session_data);
int send_server_connection_header(http2_session_data *session_data);

#endif
