/* required in cb_next_proto */
static unsigned char next_proto_list[256];
static size_t next_proto_list_len;

/* required in create_ssl_ctx */
static int cb_next_proto(SSL *s, const unsigned char **data, unsigned int *len, void *arg);
static int cb_alpn_select_proto(SSL *ssl , const unsigned char **out, unsigned char *outlen, const unsigned char *in, unsigned int inlen, void *arg);
